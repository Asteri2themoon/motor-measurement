#define MIN 1500
#define MAX 1800
#define STEP 25
#define POWER_SENSOR 9
#define OUT 10
#define FORK_SENSOR 2
#define MAX_TIME 2000
#define BUFFER_SIZE 1000

unsigned long mesures[BUFFER_SIZE];
unsigned int p;
long timeout;
volatile int timerOverflow,power=MIN;

void initRegisterMotor(){
  // Initialize Timer
  cli();          // disable global interrupts
  TCCR3A = 0;     // set entire TCCR3A register to 0
  TCCR3B = 0;     // same for TCCR3B

  OCR3A =  1250; //50Hz
  // turn on CTC mode:
  TCCR3B |= (1 << WGM32);
  // Set CS12 bits for 256 prescaler:
  TCCR3B |= (1 << CS32);
  // enable timer compare interrupt:
  TIMSK3 |= (1 << OCIE3A);
  // enable global interrupts:
  sei();
}

ISR(TIMER3_COMPA_vect)
{
  digitalWrite(OUT,HIGH);
  delayMicroseconds(power);
  digitalWrite(OUT,LOW);
}

void enableMesure(){
  cli();
  TCCR1A = 0;
  TCCR1B = 0;
  // enable Timer1 overflow interrupt:
  TIMSK1 = (1 << TOIE1);
  // prescale
  TCCR1B |= (1 << CS10);
  TCNT1=0;//reset timer
  
  timerOverflow=0;
  p=0;
  attachInterrupt(digitalPinToInterrupt(2), mesure, RISING);
  sei();
}

void disableMesure(){
  detachInterrupt(digitalPinToInterrupt(2));
}

void mesure(){
  if(p<BUFFER_SIZE){
    mesures[p]=(unsigned long)timerOverflow<<16|TCNT1;
    p++;
  }
}

ISR(TIMER1_OVF_vect)
{
  timerOverflow++;
}

void setup(){
  Serial.begin(115200);
  pinMode(OUT, OUTPUT);
  pinMode(FORK_SENSOR,INPUT);
  pinMode(POWER_SENSOR,INPUT);
  initRegisterMotor();
  delay(5000);
  Serial.println("ok");
  while(!Serial.available())delay(500);
  for(int i=1550;i<=MAX;i+=STEP){
    Serial.print("inc "+String(i)+":");
    power=MIN;
    delay(MAX_TIME);
    enableMesure();
    power=i;
    delay(MAX_TIME);
    disableMesure();
    for(int j=0;j<p;j++){
      if(j!=0){
        Serial.print(",");
      }
      Serial.print(mesures[j]);
    }
    Serial.println("");
    Serial.print("dec "+String(i)+":");
    power=MAX;
    delay(MAX_TIME);
    enableMesure();
    power=i;
    delay(MAX_TIME);
    disableMesure();
    for(int j=0;j<p;j++){
      if(j!=0){
        Serial.print(",");
      }
      Serial.print(String(mesures[j]));
    }
    Serial.println("");
  }
  power=MIN;
}

void loop(){}
